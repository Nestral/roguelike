﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New Range Attack", menuName = "Attack/RangeAttack")]
public class RangeAttack : Attack
{
    public int bulletsPerShot;
    public float spread;

    public float startBulletSpeed;
    public float bulletSpeedMultiplier;
    public float bulletLifeTime;

    public float baseDamage;

}
