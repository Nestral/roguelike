﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New Attack", menuName = "Attack/MeleeAttack")]
public class Attack : ScriptableObject
{
    public float attackTimeCooldown;

    public Vector2 entityDash;
    public float entityDashTime;

    public Transform spawnTransform;
    public Vector2 spawnTransformPosition;

}
