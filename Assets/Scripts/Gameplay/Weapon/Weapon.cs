﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Weapon : MonoBehaviour
{
    internal Entity owner;
    [HideInInspector]
    public Vector2 targetPos;

    public Attack[] attacks;
    internal float[] attackCooldownTimers;

    internal Animator weaponAnimator;

    public void SetActive(bool active)
    {
        gameObject.SetActive(active);
    }

    internal virtual void Awake()
    {
        owner = GetComponentInParent<Entity>();
        weaponAnimator = GetComponent<Animator>();
        SetupAttacks();
    }

    private void SetupAttacks()
    {
        attackCooldownTimers = new float[attacks.Length];
        for (int i = 0; i < attackCooldownTimers.Length; i++)
        {
            attackCooldownTimers[i] = attacks[i].attackTimeCooldown;
        }
    }

    internal virtual void Update()
    {
        for (int i = 0; i < attackCooldownTimers.Length; i++)
        {
            attackCooldownTimers[i] -= Time.deltaTime;
        }
    }

    public abstract void Attack(int attackIndex);

    internal virtual void Dash(Attack attack)
    {
        if (attack.entityDash != Vector2.zero)
        {
            float angle = Vector2.SignedAngle(Vector2.down, owner.facingDir);
            Vector2 dash = attack.entityDash.Rotate(angle);
            owner.StartDash(dash, attack.entityDashTime, true);
        }
    }

}
