﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Hitable : MonoBehaviour
{
    public Entity owner;
    public float damage;

    public float lifeTime;
    private float lifeTimer;
    public bool destroyOnHit;

    private void Awake()
    {
        lifeTimer = lifeTime;
    }

    private void Update()
    {
        lifeTimer -= Time.deltaTime;
        if (lifeTimer <= 0)
            Destroy(gameObject);
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.isTrigger)
            return;
        if (collision.CompareTag("Entity"))
        {
            Entity entity = collision.GetComponent<Entity>();
            bool isPlayer = entity.GetType() == typeof(Player);
            bool isOwnerPlayer = owner.GetType() == typeof(Player);

            if (isPlayer == isOwnerPlayer || entity.isInvincible)
                return;

            entity.HitEntity(transform.position, damage);            
        }
        if (destroyOnHit)
            Destroy(gameObject);
    }
}
