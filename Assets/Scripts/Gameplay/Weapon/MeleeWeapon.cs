﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MeleeWeapon : Weapon
{
    public override void Attack(int attackIndex)
    {
        if (attackIndex < attacks.Length)
        {
            UseAttack(attackIndex, "Attack" + attackIndex);
        }
    }

    private void UseAttack(int attackIndex, string animatorTriggerName)
    {
        Attack attack = attacks[attackIndex];
        float timer = attackCooldownTimers[attackIndex];

        if (timer <= 0)
        {
            if (weaponAnimator != null)
                weaponAnimator.SetTrigger(animatorTriggerName);

            // Swing
            float angle = Vector2.SignedAngle(Vector2.down, owner.facingDir);
            Vector2 swingOffset = attack.spawnTransformPosition.Rotate(angle);
            Vector2 swingPos = swingOffset + (Vector2)owner.transform.position;
            Quaternion swingRot = Quaternion.Euler(0, 0, angle);
            Transform newSwing = Instantiate(attack.spawnTransform, swingPos, swingRot);
            Hitable swing = newSwing.GetComponent<Hitable>();
            swing.owner = owner;

            Dash(attack);

            attackCooldownTimers[attackIndex] = attack.attackTimeCooldown;
        }
    }
}
