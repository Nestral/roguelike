﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RangedWeapon : Weapon
{
    Transform projectilesHolder;
    public Transform projectileSpawnpoint;    

    public int allAmmo;
    public int currentAmmo;

    private Vector2 direction;

    internal override void Awake()
    {
        base.Awake();

        projectilesHolder = GameObject.Find("Projectiles").transform;
    }

    private void Start()
    {        
        currentAmmo = allAmmo;
    }

    internal override void Update()
    {
        base.Update();

        if (owner != null)
        {
            direction = (targetPos - (Vector2)transform.position).normalized;
            RotateWeapon();
        }
    }

    private void RotateWeapon()
    {
        float rotX, angle;
        if (Vector2.SignedAngle(Vector2.up, direction) <= 0)
        {
            rotX = 0;
            angle = Vector2.SignedAngle(Vector2.right, direction);
        }
        else
        {
            rotX = 180;
            angle = -Vector2.SignedAngle(Vector2.right, direction);
        }
        Quaternion rotation = Quaternion.Euler(rotX, 0, angle);
        transform.rotation = rotation;
    }

    public override void Attack(int attackIndex)
    {
        if (attackIndex < attacks.Length)
        {
            UseAttack(attackIndex, "Shoot" + attackIndex);
        }
    }

    private void UseAttack(int attackIndex, string animatorTriggerName)
    {        
        Vector2 position = projectileSpawnpoint.position;
        RangeAttack attack = (RangeAttack)attacks[attackIndex];
        float timer = attackCooldownTimers[attackIndex];

        if (timer <= 0)
        {
            if (weaponAnimator != null)
                weaponAnimator.SetTrigger(animatorTriggerName);

            for (int i = 0; i < attack.bulletsPerShot; i++)
            {
                if (currentAmmo <= 0)
                    return;
                float angleMult = (attack.bulletsPerShot - 1) / 2f - i;
                Quaternion rotation = Quaternion.Euler(0, 0, Vector2.SignedAngle(Vector2.right, direction) + angleMult * attack.spread);

                float angle = Vector2.SignedAngle(Vector2.down, owner.facingDir);
                position += attack.spawnTransformPosition.Rotate(angle);

                Hitable newProjectile = Instantiate(attack.spawnTransform, position, rotation, projectilesHolder).GetComponent<Hitable>();
                newProjectile.lifeTime = attack.bulletLifeTime;
                newProjectile.damage = attack.baseDamage;
                newProjectile.owner = owner;

                Rigidbody2D projectileRB = newProjectile.GetComponent<Rigidbody2D>();
                projectileRB.velocity = ((Vector2)newProjectile.transform.right).normalized * attack.startBulletSpeed;

                currentAmmo--;
            }
            Dash(attack);

            attackCooldownTimers[attackIndex] = attack.attackTimeCooldown;
        }        
    }

}
