﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Controller : MonoBehaviour
{

    public float moveSpeed;

    internal Entity entity;
    internal Animator animator;
    internal Inventory inventory;

    internal virtual void Start()
    {
        entity = GetComponent<Entity>();
        animator = GetComponent<Animator>();
        inventory = GetComponent<Inventory>();
    }

}
