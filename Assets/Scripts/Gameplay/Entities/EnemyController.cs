﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyController : Controller
{    
    public Entity target;    
    public float meleeDistance = 1f;
    public float rangeDistance = 5f;
    public float distanceToPlayer;

    private Weapon meleeWeapon;
    private Weapon rangeWeapon;

    internal override void Start()
    {
        base.Start();

        meleeWeapon = FindWeapon(inventory, typeof(MeleeWeapon));
        rangeWeapon = FindWeapon(inventory, typeof(RangedWeapon));
        
        if (target == null)
        {
            target = GetTarget();
        }
    }

    private Entity GetTarget()
    {
        return FindObjectOfType<PlayerController>().GetComponent<Entity>();        
    }

    void FixedUpdate()
    {
        if (!entity.isDead)
            Act();
    }

    void Act()
    {
        if (target == null || target.currentRoom == null || target.currentRoom != entity.currentRoom)
            return;

        Vector2 targetDir = target.transform.position - transform.position;
        SetFacingDirection(targetDir.normalized);

        if (entity.canMove)
            Move();

        ChooseWeapon(targetDir.magnitude);
        Attack();
    }

    private void ChooseWeapon(float distance)
    {
        if ((NoAmmo() || distance <= meleeDistance) && meleeWeapon != null)
        {
            if (entity.currentWeapon != meleeWeapon)
                inventory.SetWeapon(meleeWeapon);
        }
        else if (distance <= rangeDistance && rangeWeapon != null)
        {
            if (entity.currentWeapon != rangeWeapon)
                inventory.SetWeapon(rangeWeapon);            
        }
    }

    private void Move()
    {
        if (moveSpeed != 0)
        {
            animator.SetBool("IsMoving", true);
            Vector3 dest = Destination();
            entity.GetComponent<Rigidbody2D>().velocity = (dest - transform.position).normalized * moveSpeed;
        }
    }

    private void Attack()
    {
        if (entity.currentWeapon != null)
        {
            entity.currentWeapon.targetPos = target.transform.position;
            entity.Attack(0);
        }
    }

    private void SetFacingDirection(Vector2 direction)
    {
        entity.facingDir = direction;
        animator.SetFloat("DirX", direction.x);
        animator.SetFloat("DirY", direction.y);
    }

    private bool NoAmmo()
    {
        if (rangeWeapon == null)
            return true;
        RangedWeapon gun = rangeWeapon.GetComponent<RangedWeapon>();
        return gun.currentAmmo <= 0;
    }

    Vector2 Destination()
    {
        if (target == null)
        {
            animator.SetBool("IsMoving", false);
            return transform.position;            
        }
        
        float distance = (target.transform.position - transform.position).magnitude;
        if (distance > distanceToPlayer)
            return target.transform.position;

        animator.SetBool("IsMoving", false);
        return transform.position;        
    }    

    private Weapon FindWeapon(Inventory inventory, System.Type type)
    {
        foreach (Weapon weapon in inventory.weapons)
        {
            if (weapon.GetType() == type)
            {
                return weapon;
            }
        }
        return null;
    }
}
