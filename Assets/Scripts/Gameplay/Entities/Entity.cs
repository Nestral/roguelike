﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Entity : MonoBehaviour
{  
    [HideInInspector]
    public Vector2 facingDir = Vector2.down;

    public Transform hitParticlesPrefab;

    public float maxHealth = 100;
    public float health;    

    public bool isInvincible;
    internal float invincibleTime;
    public float deathTime;

    [HideInInspector]
    public DungeonRoom currentRoom;
    public Weapon currentWeapon;
    internal Animator animator;
    internal Inventory inventory;

    [HideInInspector]
    public Transform rangeWeapon;
    [HideInInspector]
    public Transform meleeWeapon;

    [HideInInspector]
    public bool isDead = false;
    [HideInInspector]
    public bool canMove = true;

    private Vector2 dashDir;
    private float dashTimer;
    private float dashSpeed;

    public Transform coinPrefab;
    public DropItem[] drops;

    public List<Skill> skills;
    internal float[] skillsCooldownTimers;

    internal virtual void Awake()
    {
        animator = GetComponent<Animator>();
        inventory = GetComponent<Inventory>();        
    }    

    internal virtual void Start()
    {
        health = maxHealth;
        CheckDeath();

        SetupSkills();
    }

    private void SetupSkills()
    {
        skillsCooldownTimers = new float[skills.Count];
        for (int i = 0; i < skillsCooldownTimers.Length; i++)
        {
            skillsCooldownTimers[i] = skills[i].skillCooldown;
        }
    }

    internal virtual void Update()
    {
        for (int i = 0; i < skillsCooldownTimers.Length; i++)
        {
            skillsCooldownTimers[i] -= Time.deltaTime;
        }
    }

    public void UseSkill(int skillIndex)
    {
        if (skillIndex >= 0 && skillIndex < skills.Count)
        {
            if (skillsCooldownTimers[skillIndex] <= 0)
            {
                skills[skillIndex].UseSkill(this, Input.mousePosition);

                skillsCooldownTimers[skillIndex] = skills[skillIndex].skillCooldown;
            }
        }
    }

    public void AddSkill(Skill newSkill)
    {
        skills.Add(newSkill);
        SetupSkills();
    }

    public void RemoveSkill(Skill oldSkill)
    {
        skills.Remove(oldSkill);
        SetupSkills();
    }

    internal virtual void FixedUpdate()
    {
        if (!canMove)
        {
            GetComponent<Rigidbody2D>().velocity = dashDir * dashSpeed;

            dashTimer -= Time.deltaTime;
            if (dashTimer <= 0)
                StopDash();
        }
    }

    public void HitEntity(Vector2 hitPosition, float damage)
    {
        if (isDead || isInvincible)
            return;
        if (hitParticlesPrefab != null)
            Instantiate(hitParticlesPrefab, transform.position, Quaternion.identity);
        DealDamage(damage);
    }

    public virtual void DealDamage(float damage)
    {
        if (isDead || isInvincible)
            return;

        health -= damage;
        CheckDeath();        
    }

    public void Heal(float restore)
    {
        health += restore;
        CheckOverheal();
    }

    private void CheckDeath()
    {
        if (health <= 0)
        {
            Die();
        }
    }

    internal virtual void Die()
    {
        GetComponent<Rigidbody2D>().velocity = Vector2.zero;

        isDead = true;
        animator.SetTrigger("Die");

        DeathDrop();

        currentRoom.EnemyDies(this);
        Invoke("RemoveGO", deathTime);

        if (currentWeapon != null)
            currentWeapon.gameObject.SetActive(false);
        GetComponent<Collider2D>().enabled = false;
    }

    private void RemoveGO()
    {
        Destroy(gameObject);
    }

    private void CheckOverheal()
    {
        if (health > maxHealth)
        {
            health = maxHealth;
        }
        CheckDeath();
    }

    public void Attack(int attackIndex)
    {
        if (currentWeapon != null)
        {
            currentWeapon.Attack(attackIndex);
        }
    }

    public void StartDash(Vector2 dashDirection, float time, bool invincible)
    {
        canMove = false;
        dashTimer = time;
        dashDir = dashDirection.normalized;
        dashSpeed = dashDirection.magnitude / time;
        if (invincible)
        {
            isInvincible = true;
            invincibleTime = time;
        }
    }

    private void StopDash()
    {
        canMove = true;

    }

    private void DeathDrop()
    {
        //Drop coins
        if (coinPrefab != null)
        {
            Coin dropCoin = Instantiate(coinPrefab, transform.position, Quaternion.identity).GetComponent<Coin>();
            dropCoin.ChangeValue(inventory.money);
        }

        //Other drops
        foreach (DropItem drop in drops)
        {
            float randomChance = Random.value;
            if (randomChance <= drop.dropChance)
            {
                Transform newDropItem = Instantiate(drop.itemPrefab, transform.position, Quaternion.identity);
            }
        }
    }
}
