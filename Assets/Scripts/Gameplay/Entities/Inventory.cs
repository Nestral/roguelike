﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Inventory : MonoBehaviour
{    
    public const int numItemSlots = 10;

    public List<Weapon> weapons;
    [HideInInspector]
    public int currentWeaponIndex = 0;

    public int money;
    private Entity entity;
    private PlayerInterface playerInterface;

    private void Awake()
    {
        entity = GetComponent<Entity>();
        playerInterface = entity.GetComponent<PlayerInterface>();
    }

    public void AddWeapon(Weapon weapon)
    {
        weapons.Add(weapon);
        weapon.owner = entity;
        weapon.transform.position = entity.currentWeapon.transform.position;
        weapon.transform.SetParent(entity.rangeWeapon);
        currentWeaponIndex = weapons.Count - 1;
        SetWeapon(weapon);               
    }    

    public void ChangeWeapon(int change)
    {
        currentWeaponIndex += change;
        bool belowBound = currentWeaponIndex < 0;
        bool aboveBound = currentWeaponIndex > weapons.Count - 1;
        while (belowBound || aboveBound)
        {
            if (aboveBound)
            {
                currentWeaponIndex -= weapons.Count;
            }
            else if (belowBound)
            {
                currentWeaponIndex += weapons.Count;
            }
            belowBound = currentWeaponIndex < 0;
            aboveBound = currentWeaponIndex > weapons.Count - 1;
        }
        Weapon changeWeapon = weapons[currentWeaponIndex];
        SetWeapon(changeWeapon);
    }

    public void SetWeaponIndex(int index)
    {
        if (index > weapons.Count - 1)
            return;
        currentWeaponIndex = index;
        Weapon changeWeapon = weapons[index];
        SetWeapon(changeWeapon);
    }

    public void SetWeapon(Weapon changeWeapon)
    {
        if (changeWeapon == null)
            return;
        if (entity.currentWeapon != null)
            entity.currentWeapon.SetActive(false);
        changeWeapon.SetActive(true);
        entity.currentWeapon = changeWeapon;

        if (playerInterface != null)
        {
            playerInterface.UpdateInventoryInfo();
        }
    }
}
