﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : Controller
{
    [HideInInspector]
    public Item nearItem;

    private Vector2 movement;
    private Vector2 facingDir;    

    private void FixedUpdate()
    {
        if (!entity.isDead)
        {
            TakeInput();
            Move();
        }
    }

    private void TakeInput()
    {
        if (entity.canMove)
            TakeMovementInput();

        TakeAimInput();

        TakeAttackInput();

        TakeInventoryInput();
    }

    private void TakeMovementInput()
    {
        float horizontal = Input.GetAxisRaw("Horizontal");
        float vertical = Input.GetAxisRaw("Vertical");
        movement = new Vector2(horizontal, vertical);        
    }

    private void TakeAimInput()
    {
        //Shooting aim
        Vector2 mousePos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        facingDir = (mousePos - (Vector2)transform.position).normalized;
        SetFacingDirection(facingDir);
        if (entity.currentWeapon != null)
            entity.currentWeapon.targetPos = mousePos;                
    }    

    private void TakeAttackInput()
    {
        //Weapons
        if (Input.GetButton("Fire1"))
        {
            entity.Attack(0);
        }
        if (Input.GetButton("Fire2"))
        {
            entity.Attack(1);
        }

        //Skills
        if (Input.GetButtonDown("UseSkill0"))
        {
            entity.UseSkill(0);
        }
    }

    private void TakeInventoryInput()
    {
        if (nearItem != null && Input.GetButtonDown("PickupItem"))
        {
            nearItem.PickUp();
        }

        //Weapons
        int scrollChange = (int)Input.mouseScrollDelta.y;
        if (scrollChange != 0)
            inventory.ChangeWeapon(scrollChange);
        
        if (Input.GetKeyDown(KeyCode.Alpha1))
            inventory.SetWeaponIndex(0);
        if (Input.GetKeyDown(KeyCode.Alpha2))
            inventory.SetWeaponIndex(1);
        if (Input.GetKeyDown(KeyCode.Alpha3))
            inventory.SetWeaponIndex(2);
        if (Input.GetKeyDown(KeyCode.Alpha4))
            inventory.SetWeaponIndex(3);
        if (Input.GetKeyDown(KeyCode.Alpha5))
            inventory.SetWeaponIndex(4);
        if (Input.GetKeyDown(KeyCode.Alpha6))
            inventory.SetWeaponIndex(5);
        if (Input.GetKeyDown(KeyCode.Alpha7))
            inventory.SetWeaponIndex(6);
        if (Input.GetKeyDown(KeyCode.Alpha8))
            inventory.SetWeaponIndex(7);
        if (Input.GetKeyDown(KeyCode.Alpha9))
            inventory.SetWeaponIndex(8);
        if (Input.GetKeyDown(KeyCode.Alpha0))
            inventory.SetWeaponIndex(9);
    }

    private void Move()
    {
        //Max speed
        if (movement.magnitude > 1)
            movement = movement.normalized;
        entity.GetComponent<Rigidbody2D>().velocity = movement * moveSpeed;

        if (movement.x != 0 || movement.y != 0)
        {            
            animator.SetBool("IsMoving", true);
        }
        else
        {
            animator.SetBool("IsMoving", false);
        }        
    }

    private void SetFacingDirection(Vector2 facingDir)
    {
        entity.facingDir = facingDir;
        animator.SetFloat("DirX", facingDir.x);
        animator.SetFloat("DirY", facingDir.y);        
    }    
}