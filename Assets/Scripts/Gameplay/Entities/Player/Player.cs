﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : Entity
{
    public float maxInvincibleTime = 0.5f; //Invincibility after getting hit

    public float hitTimeScale;
    public Transform hitEffect;

    internal override void Start()
    {
        base.Start();

        invincibleTime = maxInvincibleTime;
    }

    internal override void Update()
    {
        base.Update();

        if (isInvincible)
        {
            invincibleTime -= Time.deltaTime;
            if (invincibleTime <= 0)
            {
                isInvincible = false;
                invincibleTime = maxInvincibleTime;
            }
        }
    }

    public override void DealDamage(float damage)
    {
        if (isDead || isInvincible)
            return;

        base.DealDamage(damage);        

        if (maxInvincibleTime > 0)
            isInvincible = true;

        PlayHitEffect();
    }

    internal override void Die()
    {
        isDead = true;
        animator.SetTrigger("Die");

        currentWeapon.gameObject.SetActive(false);
        GetComponent<Collider2D>().enabled = false;

        isInvincible = true;
        invincibleTime = float.MaxValue;
    }

    public void PlayHitEffect()
    {
        hitEffect.gameObject.SetActive(true);

        Time.timeScale = hitTimeScale;
        Time.fixedDeltaTime = 0.02f * Time.timeScale;
        Invoke("StopSlowMotion", maxInvincibleTime);
    }

    private void StopSlowMotion()
    {
        hitEffect.gameObject.SetActive(false);

        Time.timeScale = 1f;
        Time.fixedDeltaTime = 0.02f;
    }
}
