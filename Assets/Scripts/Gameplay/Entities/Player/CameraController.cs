﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour
{
    public Transform cameraTarget;
    public Vector2 cameraSize;
    public float cameraSpeed;
    private Vector3 offset;

    private Entity entity;    

    private void Start()
    {
        offset = cameraTarget.position - transform.position;
        entity = GetComponent<Entity>();
    }

    private void Update()
    {
        Vector3 center;
        if (entity.currentRoom == null)
            center = transform.position;
        else
            center = entity.currentRoom.transform.position;
        cameraTarget.position = Vector3.Lerp(cameraTarget.position, center + offset, cameraSpeed);
        
    }
}
