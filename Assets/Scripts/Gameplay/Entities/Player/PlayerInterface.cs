﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerInterface : MonoBehaviour
{
    public Slider healthBar;

    public Transform ammoInfo;
    public Text currentAmmoText;
    public Text allAmmoText;

    public Text moneyText;

    public Transform inventoryUI;
    private InventorySlot[] inventorySlots;
    private const int activeSlotIndex = 1;

    private Player player;
    private Inventory inventory;

    private void Awake()
    {
        player = GetComponent<Player>();
        inventory = player.GetComponent<Inventory>();
        healthBar.maxValue = player.maxHealth;        
    }

    private void Start()
    {
        inventorySlots = inventoryUI.GetComponentsInChildren<InventorySlot>();
        UpdateInventoryInfo();
    }

    private void Update()
    {        
        healthBar.value = player.health;
        moneyText.text = inventory.money.ToString();

        UpdateAmmoInfo();        
    }

    public void UpdateAmmoInfo()
    {
        if (player.currentWeapon == null)
        {
            ammoInfo.gameObject.SetActive(false);
        }
        else
        {
            RangedWeapon rangedWeapon = player.currentWeapon.GetComponent<RangedWeapon>();
            if (rangedWeapon == null)
            {
                ammoInfo.gameObject.SetActive(false);
            }
            else
            {
                ammoInfo.gameObject.SetActive(true);
                currentAmmoText.text = rangedWeapon.currentAmmo.ToString();
                allAmmoText.text = rangedWeapon.allAmmo.ToString();
            }
        }
    }

    public void UpdateInventoryInfo()
    {
        for (int i = 0; i < inventorySlots.Length; i++)
        {
            UpdateInventorySlotInfo(i);
        }
    }

    private void UpdateInventorySlotInfo(int slotIndex)
    {
        int weaponIndex = slotIndex - activeSlotIndex + inventory.currentWeaponIndex;

        if (weaponIndex >= 0 && weaponIndex < inventory.weapons.Count)
        {
            inventorySlots[slotIndex].image.sprite = inventory.weapons[weaponIndex].GetComponent<SpriteRenderer>().sprite;
            inventorySlots[slotIndex].image.enabled = true;
        }
        else
        {
            inventorySlots[slotIndex].image.enabled = false;
        }
    }
}
