﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Shape : MonoBehaviour
{

    public Vector2[] vertices;
    public Transform wallPrefab;

    public void MakeWalls()
    {
        if (vertices.Length < 2)        
            return;        

        for (int i = 0; i < vertices.Length; i++)
        {
            if (i + 1 < vertices.Length)
            {
                MakeWall(vertices[i], vertices[i + 1]);
            }
            else
            {
                MakeWall(vertices[i], vertices[0]);
            }
        }
    }

    private void MakeWall(Vector2 vertex1, Vector2 vertex2)
    {
        Vector2 position = (vertex1 + vertex2) / 2 + (Vector2)transform.position;
        Quaternion rotation = Quaternion.Euler(0, 0, Vector2.SignedAngle(Vector2.right, vertex2 - vertex1));

        Transform newWall = Instantiate(wallPrefab, position, rotation, transform);

        newWall.localScale = new Vector2((vertex2 - vertex1).magnitude, newWall.localScale.y);
    }

}
