﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Coin : Item
{
    private int value;

    private void Awake()
    {        
        ChangeSize();
    }

    internal override void CheckPick()
    {
        if (isNearPlayer)
            PickUp();
    }

    internal override void SetSprite() { }

    public void ChangeValue(int newValue)
    {
        value = newValue;
        ChangeSize();
    }

    private void ChangeSize()
    {
        if (value >= 0)
        {
            float scale = value / 2f;
            scale = Mathf.Clamp(scale, 1f, 2f);
            transform.localScale = new Vector3(scale, scale, 1f);
        }
    }

    internal override void PickUp()
    {
        Inventory inventory = nearPlayer.GetComponent<Inventory>();
        inventory.money += value;
        Destroy(gameObject);
    }

}
