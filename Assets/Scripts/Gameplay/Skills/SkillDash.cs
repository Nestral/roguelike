﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "DashSkill", menuName = "Skills/Dash")]
public class SkillDash : Skill
{
    public float maxDashDist;
    public float dashDefaultDist;

    public float dashTime;
    public float dashDamage;

    private Vector2 dashDir;

    public override void UseSkill(Entity caster, Vector2 mousePos)
    {
        dashDir = (mousePos - (Vector2)Camera.main.WorldToScreenPoint(caster.transform.position)).normalized;
        DashMove(caster, null);
    }

    private void DashMove(Entity caster, Entity ignoredEntity)
    {        
        Entity enemy = FindEnemyForDash(caster, ignoredEntity);
        if (enemy != null)
        {
            caster.StartDash(enemy.transform.position - caster.transform.position, dashTime, true);
            caster.StartCoroutine(StartDashTimer(caster, dashTime, enemy));            
        }
        else
        {
            caster.StartDash(dashDir * dashDefaultDist, dashTime, true);
        }
    }

    private Entity FindEnemyForDash(Entity caster, Entity ignoredEntity)
    {
        RaycastHit2D[] raycasts = Physics2D.RaycastAll(caster.transform.position, dashDir, maxDashDist);

        foreach (RaycastHit2D raycast in raycasts)
        {
            if (raycast.collider.CompareTag("Entity") && raycast.transform != caster.transform)
            {
                Entity enemy = raycast.transform.GetComponent<Entity>();
                if (enemy != ignoredEntity)
                    return enemy;
            }
        }

        return null;
    }

    private IEnumerator StartDashTimer(Entity caster, float time, Entity enemy)
    {
        yield return new WaitForSeconds(time);

        enemy.HitEntity(caster.transform.position, dashDamage);

        DashMove(caster, enemy);
    }
}
