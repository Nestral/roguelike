﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Skill : ScriptableObject
{

    public float skillCooldown;

    public abstract void UseSkill(Entity caster, Vector2 mousePos);

}
