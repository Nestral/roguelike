﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ShopItem : MonoBehaviour
{
    public GameObject infoObject;
    public Text priceText;

    public Weapon weapon;
    public int price;

    private bool isWeaponGiven = false;
    private bool isNearPlayer = false;
    private Player nearPlayer;

    private void Awake()
    {
        if (weapon == null)
        {
            weapon = GetComponentInChildren<Weapon>();
            if (weapon == null)
                isWeaponGiven = true;            
        }
        priceText.text = price.ToString();
    }

    private void Update()
    {
        if (isNearPlayer && Input.GetKeyDown(KeyCode.E))
        {
            Action();
        }
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (!isWeaponGiven && other.CompareTag("Entity"))
        {
            Player player = other.GetComponent<Player>();
            if (player != null)
            {
                isNearPlayer = true;
                nearPlayer = player;
                infoObject.SetActive(true);
            }
        }
    }
    private void OnTriggerExit2D(Collider2D other)
    {
        if (!isWeaponGiven && other.CompareTag("Entity"))
        {
            Player player = other.GetComponent<Player>();
            if (player != null)
            {
                isNearPlayer = false;
                nearPlayer = null;
                infoObject.SetActive(false);
            }
        }
    }

    private void Action()
    {
        Inventory inventory = nearPlayer.GetComponent<Inventory>();
        if (inventory.money >= price)
        {
            inventory.AddWeapon(weapon);
            isWeaponGiven = true;
            inventory.money -= price;

            isNearPlayer = false;
            nearPlayer = null;
            infoObject.SetActive(false);
        }
    }
}
