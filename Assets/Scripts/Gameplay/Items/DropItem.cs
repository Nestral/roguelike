﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Drop Item", menuName = "Drops")]
public class DropItem : ScriptableObject
{
    public Transform itemPrefab;
    [Range(0, 1)]
    public float dropChance;

}
