﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Medkit : Item
{

    public int restoreHealth;
    public Sprite sprite;

    internal override void SetSprite()
    {
        image.sprite = sprite;
    }

    internal override void PickUp()
    {
        if (nearPlayer.health < nearPlayer.maxHealth)
        {
            nearPlayer.Heal(restoreHealth);
            Destroy(gameObject);
        }
    }

}
