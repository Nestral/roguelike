﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public abstract class Item : MonoBehaviour
{
    public GameObject itemInfo;
    internal Image image;

    internal bool isNearPlayer;
    internal Player nearPlayer;
    private PlayerController playerController;

    private void Awake()
    {
        image = GetComponentInChildren<Image>();
    }

    private void Start()
    {
        SetSprite();
    }

    internal abstract void SetSprite();

    void Update()
    {        
        CheckPick();
    }

    internal virtual void CheckPick()
    {
        if (isNearPlayer)
        {
            playerController.nearItem = this;
        }
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.CompareTag("Entity"))
        {
            Player player = other.GetComponent<Player>();
            if (player != null)
            {
                nearPlayer = player;
                playerController = nearPlayer.GetComponent<PlayerController>();
                NearPlayer(true);
            }
        }
    }

    private void OnTriggerExit2D(Collider2D other)
    {
        if (other.CompareTag("Entity"))
        {
            Player player = other.GetComponent<Player>();
            if (player != null)
            {
                NearPlayer(false);
            }
        }
    }

    private void NearPlayer(bool isNear)
    {
        isNearPlayer = isNear;
        if (itemInfo != null)
            itemInfo.SetActive(isNear);
    }

    internal abstract void PickUp();        
}
