﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WeaponItem : Item
{
    public Weapon weapon;

    internal override void SetSprite()
    {
        image.sprite = weapon.GetComponent<SpriteRenderer>().sprite;
    }

    internal override void PickUp()
    {
        nearPlayer.inventory.AddWeapon(weapon);

        Destroy(gameObject);
    }
}
