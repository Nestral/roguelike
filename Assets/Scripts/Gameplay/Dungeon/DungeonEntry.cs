﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class DungeonEntry : MonoBehaviour
{

    public string levelName;

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.CompareTag("Entity"))
        {
            Entity entity = other.GetComponent<Entity>();
            bool isPlayer = entity.GetType() == typeof(Player);

            if (isPlayer)
            {
                SceneManager.LoadScene(levelName);
            }
        }
    }

}
