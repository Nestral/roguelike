﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DungeonRoom : MonoBehaviour
{
    public bool isSpecial;

    public Transform entitiesHolder;
    public Transform wallsHolder;
    public Transform wallPrefab;

    public Transform exitsHolder;
    public Transform exitPrefab;

    public Vector2 roomSize;
    public DungeonExit[] exits;

    public int minEnemies;
    public int maxEnemies;
    public Vector2 spawningArea;    
    public Transform[] possibleEnemies;

    [HideInInspector]
    public List<Entity> enemies;

    private bool isVisited = false;
    public float pauseTime; //Pause when player enters room for the first time
    private float leftPauseTime;

    private FinalRoom finalRoom;

    private void Awake()
    {        
        finalRoom = GetComponent<FinalRoom>();

        enemies = new List<Entity>();
    }

    private void Update()
    {
        if (leftPauseTime >= 0)
        {
            leftPauseTime -= Time.unscaledDeltaTime;
            if (leftPauseTime < 0)
                UnpauseRoom();
        }
    }

    public void SpawnEnemies()
    {
        int enemiesToSpawn = Random.Range(minEnemies, maxEnemies + 1);
        for (int i = 0; i < enemiesToSpawn; i++)
        {
            SpawnRandomEnemy();
        }
    }

    private Entity SpawnRandomEnemy()
    {
        Transform randomEnemy = GetRandomEnemy();
        Vector2 randomPos = GetRandomPos();
        Quaternion rotation = Quaternion.identity;

        Entity newEnemy = Instantiate(randomEnemy, randomPos, rotation, entitiesHolder).GetComponent<Entity>();
        return newEnemy;
    }

    private Transform GetRandomEnemy()
    {
        int randomIndex = Random.Range(0, possibleEnemies.Length);
        return possibleEnemies[randomIndex];
    }

    private Vector2 GetRandomPos()
    {
        float randomX = Random.Range(-spawningArea.x / 2, spawningArea.x / 2);
        float randomY = Random.Range(-spawningArea.y / 2, spawningArea.y / 2);
        return new Vector2(randomX, randomY) + (Vector2)transform.position;
    }

    public void CreateExits()
    {
        exits = new DungeonExit[4];
        Vector2[] dirs = new Vector2[4] { Vector2.right, Vector2.left, Vector2.up, Vector2.down };
        for (int i = 0; i < 4; i++)
        {
            Vector2 dir = dirs[i];
            Vector2 newExitPos = (Vector2)transform.position + new Vector2(dir.x * roomSize.x / 2, dir.y * roomSize.y / 2);
            Quaternion rotation = Quaternion.Euler(0, 0, Vector2.SignedAngle(Vector2.up, dir));    
            
            DungeonExit newExit = Instantiate(exitPrefab, newExitPos, rotation, exitsHolder).GetComponent<DungeonExit>();
            newExit.fromRoom = this;
            newExit.directionFromRoom = dir;
            newExit.SetActive(false);
            exits[i] = newExit;
        }
    }

    public void CloseAllExits()
    {
        foreach (DungeonExit exit in exits)
        {
            CloseExit(exit);
        }
    }

    public void CloseExit(DungeonExit exit)
    {
        exit.SetActive(false);
    }

    public void OpenAllExits()
    {
        foreach (DungeonExit exit in exits)
        {
            if (exit.toRoom != null)
                OpenExit(exit);
        }
    }

    public void OpenExit(DungeonExit exit)
    {
        exit.SetActive(true);
    }

    public DungeonExit FindExit(Vector2 direction)
    {
        foreach (DungeonExit exit in exits)
        {
            if (exit.directionFromRoom == direction)
                return exit;
        }
        return null;
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("Entity"))
        {
            Entity entity = collision.GetComponent<Entity>();
            entity.currentRoom = this;
            bool isPlayer = entity.GetType() == typeof(Player);

            if (isPlayer && !isVisited)
            {
                if (minEnemies > 0)
                    CloseAllExits();
                isVisited = true;
                PauseRoom();
            }
            else if (!isPlayer)
            {
                enemies.Add(entity);
            }
        }
    }

    public void EnemyDies(Entity enemy)
    {
        enemies.Remove(enemy);
        if (enemies.Count <= 0)
        {
            OpenAllExits();
            if (finalRoom != null)
                finalRoom.OpenExit();
        }
    }

    private void PauseRoom()
    {
        Time.timeScale = 0f;
        leftPauseTime = pauseTime;
    }

    private void UnpauseRoom()
    {
        Time.timeScale = 1f;
    }

}
