﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DungeonExit : MonoBehaviour
{
    public Transform walls;

    public DungeonRoom fromRoom;
    public Vector2 directionFromRoom;
    public DungeonRoom toRoom;

    public float spawnOffset;
    public bool isActive;

    public SpriteRenderer spriteRenderer;
    public Sprite activeSprite;
    public Sprite notActiveSprite;    

    private Vector2 nextRoomSpawn;

    private void Awake()
    {
        nextRoomSpawn = transform.position;
    }

    private void Start()
    {
        FindSpawnPoint();
    }

    public void FindSpawnPoint()
    {
        if (toRoom == null)
            return;
        DungeonExit nextRoomExit = toRoom.FindExit(-directionFromRoom);
        nextRoomSpawn = (Vector2)nextRoomExit.transform.position
            + directionFromRoom * spawnOffset;
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (isActive && collision.CompareTag("Entity"))
        {
            Entity entity = collision.GetComponent<Entity>();
            bool isPlayer = entity.GetType() == typeof(Player);

            if (isPlayer)
            {
                entity.transform.position = nextRoomSpawn;
            }
        }
    }

    public void SetActive(bool active)
    {
        isActive = active;
        if (isActive)
        {
            Open();
        }
        else
        {
            Close();
        }
    }

    private void Close()
    {
        spriteRenderer.sprite = notActiveSprite;
    }   
    
    private void Open()
    {
        spriteRenderer.sprite = activeSprite;
    }

}
