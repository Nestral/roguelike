﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DungeonGenerator : MonoBehaviour
{
    public Transform roomsHolder;

    public Transform roomPrefab;
    public Vector2 roomSize;
    public float roomExtraSpace;

    public int roomsAmount;
    private int roomsCreated;    

    public Transform startRoomPrefab;
    public Transform[] specialRoomsPrefabs;

    private DungeonRoom[] rooms;

    private void Awake()
    {
        GenerateDungeon();
    }

    public void GenerateDungeon()
    {
        Despawn();

        rooms = new DungeonRoom[roomsAmount];
        roomsCreated = 0;

        GenerateRooms();

        OpenAllExits();

        RemoveDeadExits();
    }

    private void GenerateRooms()
    {            
        SpawnInitialRoom();
        //yield return new WaitForEndOfFrame();

        while (roomsCreated < roomsAmount - specialRoomsPrefabs.Length)
        {
            DungeonRoom randomRoom = GetRandomRoom();
            DungeonExit randomExit = GetRandomExit(randomRoom);
            if (randomExit.toRoom == null)
                SpawnOrConnectNewRoom(randomRoom, randomExit);
            //yield return new WaitForEndOfFrame();
        }

        SpawnPrefabRooms();
    }

    private void SpawnPrefabRooms()
    {
        foreach (Transform specialRoomPrefab in specialRoomsPrefabs)
        {
            SpawnSpecialRoom(specialRoomPrefab);
        }
    }

    private DungeonRoom SpawnSpecialRoom(Transform prefab)
    {
        DungeonRoom newRoom;
        DungeonRoom randomRoom;
        Vector2 newRoomPos;
        do
        {            
            DungeonExit randomExit = null;
            do
            {
                randomRoom = GetRandomRoom();
                if (!randomRoom.isSpecial)
                    randomExit = GetRandomExit(randomRoom);
            } while (randomExit == null || randomExit.toRoom != null);

            Vector2 direction = randomExit.directionFromRoom;
            newRoomPos = (Vector2)randomRoom.transform.position
                + new Vector2(direction.x * (roomSize.x + roomExtraSpace), direction.y * (roomSize.y + roomExtraSpace));

            newRoom = TryFindRoom(newRoomPos);
        } while (newRoom != null);

        newRoom = SpawnNewRoom(prefab, newRoomPos);
        ConnectRooms(randomRoom, newRoom);
        return newRoom;
    }     

    private DungeonRoom GetRandomRoom()
    {
        DungeonRoom randomRoom;
        int randIndex = UnityEngine.Random.Range(0, roomsCreated);
        randomRoom = rooms[randIndex];
        return randomRoom;
    }

    private DungeonExit GetRandomExit(DungeonRoom room)
    {
        DungeonExit randomExit;
        do
        {
            int randomIndex = UnityEngine.Random.Range(0, 4);
            randomExit = room.exits[randomIndex];
        } while (randomExit == null);

        return randomExit;
    }

    private DungeonRoom SpawnInitialRoom()
    {
        Vector2 pos = transform.position;
        DungeonRoom initRoom = SpawnNewRoom(startRoomPrefab, pos);
        return initRoom;
    }

    private DungeonRoom SpawnOrConnectNewRoom(DungeonRoom neighbourRoom, DungeonExit exit)
    {
        Vector2 direction = exit.directionFromRoom;
        Vector2 newRoomPos = (Vector2)neighbourRoom.transform.position 
            + new Vector2(direction.x * (roomSize.x + roomExtraSpace), direction.y * (roomSize.y + roomExtraSpace));

        DungeonRoom newRoom = TryFindRoom(newRoomPos);
        if (newRoom == null)
        {            
            newRoom = SpawnNewRoom(roomPrefab, newRoomPos);
            //newRoom.SpawnEnemies();
        }
        ConnectRooms(neighbourRoom, newRoom);

        return newRoom;
    }

    private DungeonRoom TryFindRoom(Vector2 position)
    {
        foreach (DungeonRoom room in rooms)
        {
            if (room == null)
                return null;
            Vector2 distToRoom = position - (Vector2)room.transform.position;
            if (Mathf.Abs(distToRoom.x) <= room.roomSize.x
                && Mathf.Abs(distToRoom.y) <= room.roomSize.y)
                return room;
        }
        return null;
    }

    private DungeonRoom SpawnNewRoom(Transform prefab, Vector2 position)
    {
        DungeonRoom newRoom = Instantiate(prefab, position, Quaternion.identity, roomsHolder).GetComponent<DungeonRoom>();
        rooms[roomsCreated] = newRoom;
        roomsCreated++;
        newRoom.CreateExits();
        newRoom.SpawnEnemies();
        return newRoom;
    }

    private void ConnectRooms(DungeonRoom room1, DungeonRoom room2)
    {
        Vector2 direction1 = (room2.transform.position - room1.transform.position).normalized;
        DungeonExit exit1 = room1.FindExit(direction1);
        DungeonExit exit2 = room2.FindExit(-direction1);

        exit1.toRoom = room2;
        exit1.SetActive(true);
        exit2.toRoom = room1;
        exit2.SetActive(true);
    }

    private void OpenAllExits()
    {
        foreach (DungeonRoom room in rooms)
        {
            room.OpenAllExits();
        }
    }

    //Remove exits which don't lead anywhere
    private void RemoveDeadExits()
    {
        foreach (DungeonRoom room in rooms)
        {
            foreach (DungeonExit exit in room.exits)
            {
                if (exit.toRoom == null)
                {
                    exit.gameObject.SetActive(false);
                    SpawnWall(room, exit.transform.position, exit.transform.rotation);
                }
            }
        }
    }

    private Transform SpawnWall(DungeonRoom room, Vector2 position, Quaternion rotation)
    {
        Transform newWall = Instantiate(room.wallPrefab, position, rotation, room.wallsHolder);
        return newWall;
    }

    private void Despawn()
    {
        if (rooms == null)
            return;

        foreach (DungeonRoom room in rooms)
        {
            if (room != null)
                DestroyImmediate(room.gameObject);
        }
    }

}
