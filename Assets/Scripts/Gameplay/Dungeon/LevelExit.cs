﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LevelExit : MonoBehaviour
{
    public GameObject infoObject;
    private bool isNearPlayer = false;

    private void MoveToNextLevel()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }

    private void Update()
    {
        if (isNearPlayer && Input.GetKeyDown(KeyCode.E))
            MoveToNextLevel();
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("Entity"))
        {
            Entity entity = collision.GetComponent<Entity>();
            bool isPlayer = entity.GetType() == typeof(Player);

            if (isPlayer)
            {
                infoObject.SetActive(true);
                isNearPlayer = true;
            }
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.CompareTag("Entity"))
        {
            Entity entity = collision.GetComponent<Entity>();
            bool isPlayer = entity.GetType() == typeof(Player);

            if (isPlayer)
            {
                infoObject.SetActive(false);
                isNearPlayer = false;
            }
        }
    }

}
