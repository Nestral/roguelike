﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FinalRoom : MonoBehaviour
{
    public LevelExit levelExit;

    private void Start()
    {

        CloseExit();
    }

    public void OpenExit()
    {
        levelExit.gameObject.SetActive(true);
    }

    public void CloseExit()
    {
        levelExit.gameObject.SetActive(false);
    }
}
