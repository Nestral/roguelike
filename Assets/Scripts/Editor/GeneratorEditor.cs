﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(DungeonGenerator))]
public class GeneratorEditor : Editor
{
    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();

        DungeonGenerator script = (DungeonGenerator)target;

        if (GUILayout.Button("Generate Dungeon"))
        {
            script.GenerateDungeon();
        }
    }
}
