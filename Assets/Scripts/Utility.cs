﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class Utility
{    
    public static Vector2 Rotate(this Vector2 vector, float angle)
    {
        float radians = angle * Mathf.Deg2Rad;
        float sin = Mathf.Sin(radians);
        float cos = Mathf.Cos(radians);

        float tx = vector.x;
        float ty = vector.y;

        return new Vector2(cos * tx - sin * ty, sin * tx + cos * ty);
    }

}
